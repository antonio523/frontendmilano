import { Component, OnInit,OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AjaxService } from '../ajax.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { CocktailComponent } from '../cocktail/cocktail.component';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators'

@Component({
  selector: 'app-form-filter',
  templateUrl: './form-filter.component.html',
  styleUrls: ['./form-filter.component.css']
})
export class FormFilterComponent implements OnInit,OnDestroy {
  categorie;
  ricercaText;
  listaIngredienti;
  listaCategorie;
  listaCalici;
  elenco;
  mySubscription: any;
  form;
  form1;
  errorMessage;
  myControl = new FormControl();
  ingredients = new FormControl();
  options: string[];
  filteredOptions: Observable<string[]>;
  mySelections:[];

  closeDialog(){
    this.dialogReference.close()
  }
  changed() {
    if (this.ingredients.value.length < 4) {
      this.mySelections = this.ingredients.value;
    } else {
      this.ingredients.setValue(this.mySelections);
    }
  }

  findCocktail() {
    localStorage.removeItem('ricerca')
    this.form.reset();
    if (this.ingredients.value.length >= 1) {
      this.ajax.cocktailPerIngredienti(this.ingredients.value).subscribe(success => {
        var elenco = success;
        localStorage.setItem('ricerca', JSON.stringify(elenco))
        this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
          this.route.navigate(['/filtered']);
        });
      })
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  disableSelect = new FormControl(false);

  constructor(private ajax: AjaxService, private dialog: MatDialog, private route:Router, public dialogReference: MatDialogRef<FormFilterComponent>) { 
    this.route.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    
    this.mySubscription = this.route.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.route.navigated = false;
      }
    });
  }
  
  getAlchol(alcolico){
    localStorage.removeItem('ricerca')
    if(alcolico=="Alcoholic"){
       this.ajax.coctailAlcolici().subscribe(success=>{
         var elenco=success;
         localStorage.setItem('ricerca',JSON.stringify(elenco))
         this.route.navigate(['filtered']);
         this.dialogReference.close();
       })
    }else{
      this.ajax.coctailAnalcolici().subscribe(success=>{
        var elenco=success;
        localStorage.setItem('ricerca',JSON.stringify(elenco));
        this.route.navigate(['filtered']);
        this.dialogReference.close();
      })
    }
  }
  getPerCategoria(categoria){
    localStorage.removeItem('ricerca')
    this.ajax.cocktailPerCategoria(categoria).subscribe(success=>{
      var elenco=success;
      localStorage.setItem('ricerca',JSON.stringify(elenco))
      this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
        this.route.navigate(['/filtered']);
        this.dialogReference.close();
    }); 
    })
  }
  getPerCalice(glass){
    localStorage.removeItem('ricerca')
    this.ajax.cocktailByGlass(glass).subscribe(success=>{
      var elenco=success;
      localStorage.setItem('ricerca',JSON.stringify(elenco))
      this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
        this.route.navigate(['/filtered']);
        this.dialogReference.close();
    }); 
    })
  }
  
  submit(){
    const cocktail= this.myControl.value;
      this.ricercaText=cocktail;
      if(cocktail!=null){
        this.openDialog1(this.ricercaText)
      }else this.errorMessage="ko";
  }
  
  initForm(){
    this.form=new FormGroup({
      cocktail: new FormControl('')
    })
  }
  initForm1(){
    this.form1= new FormGroup({
      categoria: new FormControl(''),
      calice: new FormControl(''),
      alcolico: new FormControl(''),
    })
  }
  openDialog1(ricercaText){
    this.dialog.open(CocktailComponent,{
      maxHeight:'80vh',
      maxWidth: '80%',
      id: 'dialogDetail',
      data: {
        nome:ricercaText
      }
    })
  }
  ngOnInit(): void {
    this.ajax.tuttiCocktail().subscribe((success:[])=>{
      var opt=new Array();
      success.forEach((element:any) => {
        var nome= element.strDrink;
        opt.push(nome);
      });
      this.options=opt;
      this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    })
    
    this.ajax.tuttiIngredienti().subscribe(success=>{
      this.listaIngredienti=success;
    })
    this.ajax.tutteCategorie().subscribe(success=>{
      this.listaCategorie=success;
    })
    this.ajax.tuttiCalici().subscribe(success=>{
      this.listaCalici=success;
    })
    this.initForm()
    this.initForm1()
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

}

