import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BodyComponent } from './body/body.component';
import { TotalCocktailComponent } from './total-cocktail/total-cocktail.component';
import { TotalIngredientsComponent } from './total-ingredients/total-ingredients.component';
import { FiltratiComponent } from './filtrati/filtrati.component';


const routes: Routes = [
  {
    path: '',
    component: BodyComponent,
    data: { animationState: 'One' }
  },{
    path: 'allco',
    component: TotalCocktailComponent,
    data: { animationState: 'Two' }
  },{
    path: 'alling',
    component: TotalIngredientsComponent,
    data: { animationState: 'Three' }
  },{
    path: 'filtered',
    component: FiltratiComponent,
    data: { animationState: 'Four' }
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
