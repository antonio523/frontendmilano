import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { AjaxService } from '../ajax.service';
import { IngredienteComponent } from '../ingrediente/ingrediente.component';
import { RouterEvent, Router } from '@angular/router';

@Component({
  selector: 'app-cocktail',
  templateUrl: './cocktail.component.html',
  styleUrls: ['./cocktail.component.css']
})
export class CocktailComponent implements OnInit {

  immagine;
  nome;
  istruzioni;
  ingredienti = [];
  nameIngredienti;
  ingredientsLess;
  imgIngredienti;
  bicchiere;
  alcolico;
  categoria;
  ricerche;
  errore;
  allGlasses;

  constructor(private route: Router, private ajax: AjaxService, private dialogReference: MatDialogRef<CocktailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  closeDialog() {
    this.dialogReference.close()
  }

  allCocktailByIngredient(ingrediente) {
    localStorage.removeItem('ricerca')
    this.ajax.cocktailPerIngrediente(ingrediente).subscribe(success => {
      let elenco = success;
      console.log(success)
      localStorage.setItem('ricerca', JSON.stringify(elenco))
      this.dialogReference.close()
      this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
        this.route.navigate(['/filtered']);
      });
    })
  }

  ngOnInit(): void {
    this.ajax.cocktailByName(this.data.nome).subscribe((success: any) => {
      if (success == null) {
        this.errore = true
      } else {
        console.log(success)
        this.immagine = success.strDrinkThumb;
        this.nome = success.strDrink;
        this.istruzioni = success.strInstructions;
        this.nameIngredienti = success.ingredientsFull;
        this.ingredientsLess = success.ingredientsLess;
        this.imgIngredienti = success.imgIngredienti;
        this.bicchiere = success.strGlass;
        this.alcolico = success.strAlcoholic;
        this.categoria = success.strCategory;
        this.ricerche = success.counter;
        for (let x = 0; x < this.nameIngredienti.length; x++) {
          this.ingredienti.push({
            nameFull: this.nameIngredienti[x],
            nameLess: this.ingredientsLess[x],
            img: this.imgIngredienti[x]
          })
        }
      }
    })
  }

}
