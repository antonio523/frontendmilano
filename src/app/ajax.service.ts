import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  constructor(private http: HttpClient) { }

  cocktailDelGiorno(){
    return this.http.get("http://localhost:8080/cocktailmilano/cocktailDelGiornoServlet.json");
  }
  cocktailRandom(){
    return this.http.get("http://localhost:8080/cocktailmilano/randomCocktailServlet.json");
  }
  coctailPopolari(){
    return this.http.get("http://localhost:8080/cocktailmilano/cocktailPiuPopolariServlet.json");
  }
  ingredientiPopolari(){
    return this.http.get("http://localhost:8080/cocktailmilano/ingredientiPiuPopolariServlet.json");
  }
  coctailAlcolici(){
    return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailAlcoliciServlet.json");
  }
  coctailAnalcolici(){
    return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailAnalcolici.json");
  }
  coctailPeriniziale(lettera){
    return this.http.get("http://localhost:8080/cocktailmilano/cocktailRicercaAlfabetica.json?iniziale="+lettera);
  }
  ingredientePeriniziale(lettera){
    return this.http.get("http://localhost:8080/cocktailmilano/ingredienteRicercaAlfabetica.json?iniziale="+lettera);
  }
  cocktailPerCategoria(categoria){
    return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailPerCategoriaServlet.json?type="+categoria);
  }
  cocktailPerIngredienti(ing){
    if(ing.length==1) return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailPerIngredientiServlet.json?ing1="+ing[0]);
    else if(ing.length==2) return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailPerIngredientiServlet.json?ing1="+ing[0]+"&ing2="+ing[1]);
    else return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailPerIngredientiServlet.json?ing1="+ing[0]+"&ing2="+ing[1]+"&ing3="+ing[2]);
  }
  cocktailPerIngrediente(ing1){
    return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailPerIngredientiServlet.json?ing1="+ing1)}
    
  cocktailByGlass(name){
    return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailPerBicchiereServlet.json?type="+name);
  }
  cocktailByName(nome:String){
    let name=nome.replace("&","%26");
    return this.http.get("http://localhost:8080/cocktailmilano/ricercaCocktailPerNome.json?type="+name);
  }
  ingredienteByName(nome){
    return this.http.get("http://localhost:8080/cocktailmilano/ricercaIngredienti.json?type="+nome);
  }
  inizialiCocktail(){
    return this.http.get("http://localhost:8080/cocktailmilano/getInizialiCocktail.json");
  }
  inizialiIngredienti(){
    return this.http.get("http://localhost:8080/cocktailmilano/getInizialiIngredienti.json");
  }
  tuttiCocktail(){
    return this.http.get("http://localhost:8080/cocktailmilano/allCocktail.json");
  }
  tuttiIngredienti(){
    return this.http.get("http://localhost:8080/cocktailmilano/allIngredients.json");
  }
  tutteCategorie(){
    return this.http.get("http://localhost:8080/cocktailmilano/allCategorie.json");
  }
  tuttiCalici(){
    return this.http.get("http://localhost:8080/cocktailmilano/allGlasses.json");
  }

}
