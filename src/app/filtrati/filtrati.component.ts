import { Component, OnInit } from '@angular/core';
import { AjaxService } from '../ajax.service';
import { CocktailComponent } from '../cocktail/cocktail.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-filtrati',
  templateUrl: './filtrati.component.html',
  styleUrls: ['./filtrati.component.css']
})
export class FiltratiComponent implements OnInit {
  cocktailList;
  cocktailListTemp;
  iniziali;
  page=1;
  pageSize=12;
  employeedata = [];
  // Pagination parameters.
  count = 5;

  constructor(private ajax: AjaxService,private dialog: MatDialog) { }

  forLetter(lettera){
    let list=[];
    for(let x=0;x<this.cocktailList.length;x++){
      console.log(this.cocktailList[x])
      if(this.cocktailList[x].strDrink.startsWith(lettera)){
        list.push(this.cocktailList[x]);
      }
    }this.cocktailListTemp=list;
  }
  ngOnInit(): void {
    this.cocktailList=JSON.parse(localStorage.getItem("ricerca"));
    this.cocktailListTemp=this.cocktailList;
    console.log(this.cocktailList)
    this.iniziali=this.generaIniziali(this.cocktailListTemp);
  }
  generaIniziali(lista:any){
    let lettere=[];
    for(let x=0;x<lista.length;x++){
      if(!lettere.includes(lista[x].strDrink.charAt(0))){
        lettere.push(lista[x].strDrink.charAt(0))
      }
    }return lettere;
  }
  openDialog1(cocktail){
    this.dialog.open(CocktailComponent,{
      maxHeight:'80vh',
      maxWidth: '80%',
      id: 'dialogDetail',
      data: {
        nome:cocktail
      }
    })
  }

}

