import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltratiComponent } from './filtrati.component';

describe('FiltratiComponent', () => {
  let component: FiltratiComponent;
  let fixture: ComponentFixture<FiltratiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltratiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltratiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
