import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatDialogModule} from '@angular/material/dialog'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AjaxService } from './ajax.service';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { DetailsComponent } from './details/details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule }  from '@angular/material/input';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IngredienteComponent } from './ingrediente/ingrediente.component';
import { CocktailComponent } from './cocktail/cocktail.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TotalCocktailComponent } from './total-cocktail/total-cocktail.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { TotalIngredientsComponent } from './total-ingredients/total-ingredients.component';
import { ChartsModule } from 'ng2-charts';
import { FormFilterComponent } from './form-filter/form-filter.component';
import { FiltratiComponent } from './filtrati/filtrati.component';
import { MatSelectModule } from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    DetailsComponent,
    IngredienteComponent,
    CocktailComponent,
    TotalCocktailComponent,
    TotalIngredientsComponent,
    FormFilterComponent,
    FiltratiComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatPaginatorModule,
    ChartsModule,
    MatSelectModule,
    MatAutocompleteModule
    
  ],
  providers: [
    AjaxService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
