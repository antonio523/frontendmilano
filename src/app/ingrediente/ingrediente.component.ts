import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AjaxService } from '../ajax.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingrediente',
  templateUrl: './ingrediente.component.html',
  styleUrls: ['./ingrediente.component.css']
})
export class IngredienteComponent implements OnInit {
  immagine;
  nome;
  descrizione;
  ricerche;
  
  constructor(private ajax:AjaxService, private dialogReference: MatDialogRef<IngredienteComponent>,
  @Inject(MAT_DIALOG_DATA) public data:any,private route:Router
  ) { }
  closeDialog(){
    this.dialogReference.close()
  }
  ngOnInit(): void {
    this.ajax.ingredienteByName(this.data.nome).subscribe((success:any)=>{
      console.log(success);
      this.immagine=success.immagine;
      this.nome=success.strIngredient;
      this.descrizione=success.strDescription;
      this.ricerche=success.counter;
    })
  }
  allCocktailByIngredient(){
    localStorage.removeItem('ricerca')
    this.ajax.cocktailPerIngrediente(this.nome).subscribe(success=>{
      let elenco=success;
      console.log(success)
      localStorage.setItem('ricerca',JSON.stringify(elenco))
      this.dialogReference.close()
      this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
        this.route.navigate(['/filtered']);
    }); 
    })
  }

}




