import { Component, OnInit } from '@angular/core';
import { CocktailComponent } from '../cocktail/cocktail.component';
import { AjaxService } from '../ajax.service';
import { MatDialog } from '@angular/material/dialog';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-total-cocktail',
  templateUrl: './total-cocktail.component.html',
  styleUrls: ['./total-cocktail.component.css']
})
export class TotalCocktailComponent implements OnInit {

  cocktailList;
  iniziali;

  // Pagination parameters.
  page=1;
  pageSize=12;
  employeedata = [];
  count = 5;
  
  constructor(private ajax: AjaxService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.ajax.tuttiCocktail().subscribe((response) => {
      this.cocktailList = response; 
    })
    this.ajax.inizialiCocktail().subscribe((response)=>{
      this.iniziali = response;
    })
  }
  
  openDialog1(cocktail){
    this.dialog.open(CocktailComponent,{
      maxHeight:'80vh',
      maxWidth: '80%',
      id: 'dialogDetail',
      data: {
        nome:cocktail
      }
    })
  }
  
  forLetter(lettera){
    this.ajax.coctailPeriniziale(lettera).subscribe((response)=>{
      this.cocktailList=response;
    })
  }

}
