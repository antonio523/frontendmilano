import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalCocktailComponent } from './total-cocktail.component';

describe('TotalCocktailComponent', () => {
  let component: TotalCocktailComponent;
  let fixture: ComponentFixture<TotalCocktailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalCocktailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalCocktailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
