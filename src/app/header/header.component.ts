import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CocktailComponent } from '../cocktail/cocktail.component';
import { AjaxService } from '../ajax.service';
import { FormFilterComponent } from '../form-filter/form-filter.component';
import { MatDialog } from '@angular/material/dialog';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  mySubscription: any;
  constructor(private ajax: AjaxService, private dialog: MatDialog,private route:Router, private dialog2: MatDialog, private dialog1: MatDialog) {
    this.route.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.route.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        
        this.route.navigated = false;
      }
    });
   }
  ricercaText;
  listaIngredienti;
  listaCategorie;
  listaCalici;
  form;
  form1;
  errorMessage;
  options: string[];
  disableSelect = new FormControl(false);
  myControl = new FormControl();
  ingredients = new FormControl();
  filteredOptions: Observable<string[]>;
  mySelections:[];
  getAlchol(alcolico){
    localStorage.removeItem('ricerca')
    if(alcolico=="Alcoholic"){
       this.ajax.coctailAlcolici().subscribe(success=>{
         var elenco=success;
         localStorage.setItem('ricerca',JSON.stringify(elenco))
         this.route.navigate(['filtered']);
         this.form1.reset()
       })
    }else{
      this.ajax.coctailAnalcolici().subscribe(success=>{
        var elenco=success;
        localStorage.setItem('ricerca',JSON.stringify(elenco));
        this.route.navigate(['filtered']);
        this.form1.reset()
      })
    }
  }
  getPerCategoria(categoria){
    localStorage.removeItem('ricerca')
    this.ajax.cocktailPerCategoria(categoria).subscribe(success=>{
      var elenco=success;
      localStorage.setItem('ricerca',JSON.stringify(elenco))
      this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
        this.route.navigate(['/filtered']);
        this.form1.reset()
    }); 
    })
  }
  getPerCalice(glass){
    localStorage.removeItem('ricerca')
    this.ajax.cocktailByGlass(glass).subscribe(success=>{
      var elenco=success;
      localStorage.setItem('ricerca',JSON.stringify(elenco))
      this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
        this.route.navigate(['/filtered']);
        this.form1.reset()
    }); 
    })
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  changed() {
    if (this.ingredients.value.length < 4) {
      console.log(this.ingredients.value)
      this.mySelections = this.ingredients.value;
    } else {
      this.ingredients.setValue(this.mySelections);
    }
  }

  findCocktail() {
    localStorage.removeItem('ricerca')
    this.form.reset();
    if (this.ingredients.value.length >= 1) {
      this.ajax.cocktailPerIngredienti(this.ingredients.value).subscribe(success => {
        var elenco = success;
        localStorage.setItem('ricerca', JSON.stringify(elenco))
        this.route.navigateByUrl('/filtered', { skipLocationChange: true }).then(() => {
          this.route.navigate(['/filtered']);
        });
      })
    }
  }
  /*showFilter(){
    if(this.filterOn==true) this.filterOn=false;
    else this.filterOn=true;
  }*/
  initForm(){
    this.form=new FormGroup({
      cocktail: new FormControl('')
    })
  }
  openDialog1(ricercaText){
    this.dialog.open(CocktailComponent,{
      maxHeight:'80vh',
      maxWidth: '80%',
      id: 'dialogDetail',
      data: {
        nome:ricercaText
      }
    })
  }
  openDialog(){
    this.dialog1.open(FormFilterComponent,{
      maxHeight:'80vh',
      maxWidth: '90%',
      minWidth:"50%",
      id: 'formDetail'
    })
  }
  initForm1(){
    this.form1= new FormGroup({
      categoria: new FormControl(''),
      calice: new FormControl(''),
      alcolico: new FormControl(''),
      ingrediente: new FormControl(''),
    })
  }
  submit(){
    const cocktail= this.myControl.value;
      this.ricercaText=cocktail;
      if(cocktail!=null){
        this.openDialog1(this.ricercaText)
      }else this.errorMessage="ko";
  }
  ngOnInit(): void {
    this.ajax.tuttiCocktail().subscribe((success:[])=>{
      var opt=new Array();
      success.forEach((element:any) => {
        var nome= element.strDrink;
        opt.push(nome);
      });
      this.options=opt;
      this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    })
    this.ajax.tuttiIngredienti().subscribe(success=>{
      this.listaIngredienti=success;
    })
    this.ajax.tutteCategorie().subscribe(success=>{
      this.listaCategorie=success;
    })
    this.ajax.tuttiCalici().subscribe(success=>{
      this.listaCalici=success;
    })
    this.initForm()
    this.initForm1()
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }
}