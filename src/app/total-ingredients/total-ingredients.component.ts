import { Component, OnInit } from '@angular/core';
import { AjaxService } from '../ajax.service';
import { MatDialog } from '@angular/material/dialog';
import { IngredienteComponent } from '../ingrediente/ingrediente.component';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-total-ingredients',
  templateUrl: './total-ingredients.component.html',
  styleUrls: ['./total-ingredients.component.css']
})
export class TotalIngredientsComponent implements OnInit {

  IngredientList;
  iniziali;

  // Pagination parameters.
  page=1;
  pageSize=12;
  employeedata = [];
  count = 5;

  constructor(private ajax: AjaxService, private dialog: MatDialog,private dialog2: MatDialog) { }

  ngOnInit(): void {
    this.ajax.tuttiIngredienti().subscribe((response)=>{
      this.IngredientList=response;
    })
    this.ajax.inizialiIngredienti().subscribe((response)=>{
      this.iniziali = response;
    })
  }

  openDialog2(ingrediente){
    this.dialog2.open(IngredienteComponent,{
      maxHeight:'80vh',
      maxWidth: '80%',
      id: 'dialogDetail',
      data: {
        nome:ingrediente
      }
    })
  }

  forLetter(lettera){
    this.ajax.ingredientePeriniziale(lettera).subscribe((response)=>{
      this.IngredientList=response;
    })
  }
  
}
