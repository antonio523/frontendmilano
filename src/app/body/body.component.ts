import { Component, OnInit, Inject } from '@angular/core';
import { AjaxService } from '../ajax.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CocktailComponent } from '../cocktail/cocktail.component';
import { IngredienteComponent } from '../ingrediente/ingrediente.component';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
// For MDB Angular Free


@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  constructor(private ajax: AjaxService, private dialog: MatDialog, private dialog2: MatDialog) { }


  
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels = ["Sicilia", "Piemonte", "Marche", "Valle d'Aosta", "Toscana", "Campania", "Puglia", "Veneto", "Lombardia", "Emilia-Romagna", "Trentino-Alto Adige", "Sardegna", "Molise", "Calabria", "Abruzzo", "Lazio", "Liguria", "Friuli-Venezia Giulia", "Basilicata", "Umbria"];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData= [
    { data: [], label: 'Totale cocktail' }
  ];



  barChartOptions2: ChartOptions = {
    responsive: true,
  };
  barChartLabels2 = ["Sicilia", "Piemonte", "Marche", "Valle d'Aosta", "Toscana", "Campania", "Puglia", "Veneto", "Lombardia", "Emilia-Romagna", "Trentino-Alto Adige", "Sardegna", "Molise", "Calabria", "Abruzzo", "Lazio", "Liguria", "Friuli-Venezia Giulia", "Basilicata", "Umbria"];
  barChartType2: ChartType = 'bar';
  barChartLegend2 = true;
  barChartPlugins2 = [];

  barChartData2= [
    { data: [], label: 'Totale cocktail', }
  ];




  cocktailList:any;
  ingredientsList;
  dailyCocktail;
  randomCocktail;
  openDialog1(cocktail){
    this.dialog.open(CocktailComponent,{
      maxHeight:'90vh',
      maxWidth: '80%',
      id: 'dialogDetail',
      data: {
        nome:cocktail
      }
    })
  }
  randomDrink(){
    this.ajax.cocktailRandom().subscribe(response =>{
      this.randomCocktail = response;
    })
  }
  openDialog2(ingrediente){
    this.dialog2.open(IngredienteComponent,{
      maxHeight:'90vh',
      maxWidth: '80%',
      id: 'dialogDetail',
      data: {
        nome:ingrediente
      }
    })
  }
  creaValori(arrayDati:any){
    let arrayLabel=new Array();
    let arrayValue=new Array();
    for(let x=0;x<arrayDati.length;x++){
      arrayLabel.push(arrayDati[x].strDrink);
      arrayValue.push(arrayDati[x].counter);
    }
    this.barChartData=[
      { data: arrayValue, label: 'Most viewed cocktails' }
    ]
    this.barChartLabels=arrayLabel;
  }
  creaValori2(arrayDati:any){
    let arrayLabel=new Array();
    let arrayValue=new Array();
    for(let x=0;x<arrayDati.length;x++){
      arrayLabel.push(arrayDati[x].strIngredient);
      arrayValue.push(arrayDati[x].counter);
    }
    this.barChartData2=[
      { data: arrayValue, label: 'Most viewed ingredients' }
    ]
    this.barChartLabels2=arrayLabel;
  }
  ngOnInit(): void {
    let rank:[]
      this.ajax.coctailPopolari().subscribe((response) => {
        this.cocktailList = response;
        this.creaValori(response);
      })
      this.ajax.ingredientiPopolari().subscribe(response=>{
        this.ingredientsList= response;
        this.creaValori2(response);
      })
      this.ajax.cocktailDelGiorno().subscribe(response =>{
        this.dailyCocktail = response;
      })
      this.ajax.cocktailRandom().subscribe(response =>{
        this.randomCocktail = response;
      })
  }
}
