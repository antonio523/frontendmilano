  /*import { trigger, transition, style, query, animate, animateChild, group } from '@angular/animations';
export const routeTransitionAnimations = trigger('triggerName', [
    
    transition('One => Two,One => Three,One => Four, One <=> One', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          bottom: 0,
          width: '100%'
        })
      ]),
      query(':enter', [style({ bottom: '100%', opacity: 0 })]),
      query(':leave', animateChild()),
      group([
        query(':leave', [animate('0.6s ease-out', style({ top: '100%', opacity: 0 }))]),
        query(':enter', [animate('0.6s ease-out', style({ bottom: '0%', opacity: 1 }))])
       ]),
       query(':enter', animateChild())
     ]),
     transition('Three => One,Two => One,Four => One' , [
        style({ position: 'relative' }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: 0,
            bottom: 0,
            width: '100%'
          })
        ]),
        query(':enter', [style({ bottom: '-100%', opacity: 0 })]),
        query(':leave', animateChild()),
        group([
          query(':leave', [animate('0.6s ease-out', style({ top: '-100%', opacity: 0 }))]),
          query(':enter', [animate('0.6s ease-out', style({ bottom: '0%', opacity: 1 }))])
         ]),
         query(':enter', animateChild())
       ])
  ]);*/

  import { trigger, transition, style, query, animate, animateChild, group } from '@angular/animations';
export const routeTransitionAnimations = trigger('triggerName', [
    
    transition('One => Two,One => Three,One => Four, One <=> One, Two => Four, Three <=> Four', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          right: 0,
          width: '100%'
        })
      ]),
      query(':enter', [style({ left: '100%', opacity: 0 })]),
      query(':leave', animateChild()),
      group([
        query(':leave', [animate('0.4s ease-out', style({ right: '100%', opacity: 0 }))]),
        query(':enter', [animate('0.4s ease-out', style({ left: '0%', opacity: 1 }))])
       ]),
       query(':enter', animateChild())
     ]),
     transition('Three => One,Two => One,Four => One' , [
        style({ position: 'relative' }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: 0,
            right: 0,
            width: '100%'
          })
        ]),
        query(':enter', [style({ left: '-100%', opacity: 0 })]),
        query(':leave', animateChild()),
        group([
          query(':leave', [animate('0.4s ease-out', style({ right: '-100%', opacity: 0 }))]),
          query(':enter', [animate('0.4s ease-out', style({ left: '0%', opacity: 1 }))])
         ]),
         query(':enter', animateChild())
       ])
  ]);