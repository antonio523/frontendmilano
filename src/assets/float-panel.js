window.onscroll = function() {scrollFunctionDesktop(),scrollFunctionMobile()};

function scrollFunctionDesktop() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.padding = "10px 10px";
    document.getElementById('navBasic').style.display= "none";
  } else {
    document.getElementById("navbar").style.padding = "30px 10px";
    document.getElementById('navBasic').style.display= "block";
  }
}
function scrollFunctionMobile() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbarMobile").style.padding = "10px 10px";
  } else {
    document.getElementById("navbarMobile").style.padding = "30px 10px";
  }
}